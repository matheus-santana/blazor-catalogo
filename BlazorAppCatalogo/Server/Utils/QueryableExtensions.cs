﻿using System.Linq;
using BlazorAppCatalogo.Shared.Resources;

namespace BlazorAppCatalogo.Server.Utils
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> Paginate<T>(this IQueryable<T> queryable, Pagination pagination)
        {
            return queryable.Skip((pagination.Page - 1) * pagination.Items).Take(pagination.Items);
        }
    }
}
