﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BlazorAppCatalogo.Server.Utils
{
    public static class HttpContextExtensions
    {
        public async static Task InsertParameterInPageResponse<T>(this HttpContext context,
            IQueryable<T> queryable, int totalItemsToShow)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            double totalItems = await queryable.CountAsync();
            double totalPages = Math.Ceiling(totalItems / totalItemsToShow);

            context.Response.Headers.Add("totalItems", totalItems.ToString());
            context.Response.Headers.Add("totalPages", totalPages.ToString());
        }
    }
}
