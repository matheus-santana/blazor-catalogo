﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorAppCatalogo.Server.Context;
using BlazorAppCatalogo.Server.Utils;
using BlazorAppCatalogo.Shared.Models;
using BlazorAppCatalogo.Shared.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlazorAppCatalogo.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly DBContext context;

        public ProductController(DBContext context)
        {
            this.context = context;
        }

        [HttpGet("categories/{id:int}")]
        public async Task<ActionResult<List<Product>>> GetByCategory(int id)
        {
            return await context.Products.Where(p => p.CategoryId == id).ToListAsync();
        }

        [HttpGet("all")]
        public async Task<ActionResult<List<Product>>> Get()
        {
            return await context.Products.AsNoTracking().ToListAsync();
        }

        [HttpGet]
        public async Task<ActionResult<List<Product>>> Get([FromQuery] Pagination pagination,
            [FromQuery] string name)
        {
            var queryable = context.Products.AsQueryable();

            if (!string.IsNullOrEmpty(name))
            {
                queryable = queryable.Where(x => x.Name.Contains(name));
            }

            await HttpContext.InsertParameterInPageResponse(queryable, pagination.Items);

            return await queryable.Paginate(pagination).ToListAsync();
            //return await context.Categories.AsNoTracking().ToListAsync();
        }

        [HttpGet("{id}", Name = "GetProduct")]
        public async Task<ActionResult<Product>> Get(int id)
        {
            return await context.Products.FirstOrDefaultAsync(x => x.ProductId == id);
        }

        [HttpPost]
        public async Task<ActionResult<Product>> Post(Product product)
        {
            context.Add(product);
            await context.SaveChangesAsync();
            return new CreatedAtRouteResult("GetProduct", new { id = product.ProductId }, product);
        }

        [HttpPut]
        public async Task<ActionResult<Product>> Put(Product product)
        {
            context.Entry(product).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return Ok(product);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Product>> Delete(int id)
        {
            var product = new Product { ProductId = id };
            context.Remove(product);
            await context.SaveChangesAsync();
            return Ok(product);
        }
    }
}
