﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorAppCatalogo.Server.Context;
using BlazorAppCatalogo.Server.Utils;
using BlazorAppCatalogo.Shared.Models;
using BlazorAppCatalogo.Shared.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlazorAppCatalogo.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly DBContext context;

        public CategoryController(DBContext context)
        {
            this.context = context;
        }

        [HttpGet("all")]
        public async Task<ActionResult<List<Category>>> Get()
        {
            return await context.Categories.AsNoTracking().ToListAsync();
        }

        [HttpGet]
        public async Task<ActionResult<List<Category>>> Get([FromQuery] Pagination pagination,
            [FromQuery] string name)
        {
            var queryable = context.Categories.AsQueryable();

            if (!string.IsNullOrEmpty(name))
            {
                queryable = queryable.Where(x => x.Name.Contains(name));
            }

            await HttpContext.InsertParameterInPageResponse(queryable, pagination.Items);

            return await queryable.Paginate(pagination).ToListAsync();
            //return await context.Categories.AsNoTracking().ToListAsync();
        }

        [HttpGet("{id}", Name = "GetCategory")]
        public async Task<ActionResult<Category>> Get(int id)
        {
            return await context.Categories.FirstOrDefaultAsync(x => x.CategoryId == id);
        }

        [HttpPost]
        public async Task<ActionResult<Category>> Post(Category category)
        {
            context.Add(category);
            await context.SaveChangesAsync();
            return new CreatedAtRouteResult("GetCategory", new { id = category.CategoryId }, category);
        }

        [HttpPut]
        public async Task<ActionResult<Category>> Put(Category category)
        {
            context.Entry(category).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return Ok(category);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Category>> Delete(int id)
        {
            var category = new Category { CategoryId = id };
            context.Remove(category);
            await context.SaveChangesAsync();
            return Ok(category);
        }
    }
}
