﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BlazorAppCatalogo.Server.Migrations
{
    public partial class CreateRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "954d785e-b456-4c7e-b9b7-f3f0fb1f1c2c", "fd70eb6f-205b-483a-8420-13790e4d0762", "User", "USER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "e17bdbef-4c6a-4fb5-8989-79d915d11499", "edd027a8-b412-40c7-bd72-eab937881a77", "Admin", "ADMIN" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "954d785e-b456-4c7e-b9b7-f3f0fb1f1c2c");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e17bdbef-4c6a-4fb5-8989-79d915d11499");
        }
    }
}
