﻿namespace BlazorAppCatalogo.Shared.Resources
{
    public class Pagination
    {
        public int Page { get; set; } = 1;
        public int Items { get; set; } = 5;
    }
}
