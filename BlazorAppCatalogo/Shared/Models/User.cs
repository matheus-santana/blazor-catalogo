﻿using System.ComponentModel.DataAnnotations;

namespace BlazorAppCatalogo.Shared.Models
{
    public class User
    {

        [Required(ErrorMessage = "Informe o e-mail!")]
        [EmailAddress(ErrorMessage = "Formato de e-mail inválido!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Informe a senha!")]
        public string Password { get; set; }
    }
}
