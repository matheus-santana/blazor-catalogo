﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlazorAppCatalogo.Shared.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Informe o nome do produto")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Informe a descrição do produto")]
        [MaxLength(200)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Informe o preço do produto")]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }

        public string ImageUrl { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }
    }
}
