﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlazorAppCatalogo.Shared.Models
{
    public class Category
    {
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "O nome da categoria é obrigatório.")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "A descrição da categoria é obrigatório.")]
        [MaxLength(200)]
        public string Description { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
