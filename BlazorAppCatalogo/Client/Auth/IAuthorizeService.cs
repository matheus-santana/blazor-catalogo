﻿using System.Threading.Tasks;

namespace BlazorAppCatalogo.Client.Auth
{
    public interface IAuthorizeService
    {
        Task Login(string token);
        Task Logout();
    }
}
