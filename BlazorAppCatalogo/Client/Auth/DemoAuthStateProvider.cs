﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Authorization;

namespace BlazorAppCatalogo.Client.Auth
{
    public class DemoAuthStateProvider : AuthenticationStateProvider
    {
        public async override Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            await Task.Delay(4000);
            //Indicar se usuário está autenticado e seus claims
            var usuario = new ClaimsIdentity(new List<Claim>() {
                new Claim("Chave", "Valor"),
                new Claim(ClaimTypes.Name, "Matheus Santana"),
                new Claim(ClaimTypes.Role, "Admin")
            }, "demo");

            return await Task.FromResult(new AuthenticationState(
                new ClaimsPrincipal(usuario)));
        }
    }
}
